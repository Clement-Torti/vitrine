# Vitrine

"Internet application" project of the 2nd year in ENSEEIHT.
This site is a product selling platform, developed with the technologies learn during the lecture.


# Members of the team : 

KLEINE Killian
LOPES Philippe
POUDRET Mederick
TORTI Clement

# Project status

## Current limitations
The account balance is not yet automatically refreshed. A reload of the page is needed.
In order to have working categories, they have to be manually putted in the database. 
Once the backend is installed and ready to be started, use the folowing commands and replace the brackets <> with the name of the category you want to add.
```
sudo su postgres
psql vitrine
INSERT INTO category ('label') VALUES '<name_of_the_category>';
```

## Project future
We did not have the time to implement all the functionnalities in our project due to the deadline.
Because it is a school project it will not be maintained or have further developpement

# Installation

## Install backend
```
cd backend
npm install
```

## Create tables
```
cd backend

sudo apt install postgresql # install postgres
sudo su postgres
psql
createdb vitrine # create local database

ctrl + d
ctrl + d

npm run create-tables # run table generator

cd ../frontend
```

## add tables
```
add .model and .service files in backend/src/services/dabase
add them in scripts/create-tables
run npm create-tables
add them in /services/index
```
## Install frontend
```
cd frontend
npm install
```


# Execution

## Start server
```
cd backend
npm run dev
```

## Start client
```
cd frontend
npm run serve
```

Open browser on localhost:8080
Open multiple tabs to see real-time effects

## Authors and acknowledgment
It is important to note that thsi project required a lot of peer programming.
Thus if a person did not commit a lot of work, this person should have been working with other members of the team.


